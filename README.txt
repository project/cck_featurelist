﻿README.txt
----------
Featurelist provides a configurable table of feature states, for example your
products or services features. You may check or negate a feature and set an
optional text for each.

INSTALLATION
------------
- Copy featurelist directory to /sites/all/modules
- Enable module at /admin/build/modules
- Add a featurelist to a content type at /admin/content/node-type/[type]/fields
- If you like to, you may customize settings at /admin/settings/featurelist

AUTHOR/MAINTAINER
-----------------
- Julian Pustkuchen (http://Julian.Pustkuchen.com)
- Thomas Frobieter (http://blog.frobieter.com)

The module is based on the "tablefield" module written by
Kevin Hankens (http://www.kevinhankens.com).
